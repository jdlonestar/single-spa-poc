import { start, registerApplication, navigateToUrl } from 'single-spa'
import * as singleSpa from 'single-spa'
import { Subject } from 'rxjs';

const hashPrefix = prefix => location => location.hash.startsWith(`#${prefix}`)

// registerApplication('react', () => import('../react/index.js'), hashPrefix('/react'))
// registerApplication('angular', () => import('../angular/index.js'), hashPrefix('/angular'))

var authToken = {
    authToken: '11111111111'
}
let authSubject = new Subject(authToken);
// registerApplication('my-app', () => System.import('my-app'), hashPrefix('/app1'));
registerApplication('my-app', () => scriptTagAdd('http://localhost:4200/main.js', 'my-app'), hashPrefix('/app1'), authToken);
registerApplication('my-app2', () => scriptTagAdd('http://localhost:4201/main.js', 'my-app2'), hashPrefix('/app2'));

function activeWhen() {
    return window.location.pathname.indexOf('/app1') === 0;
}

start()



//navigateToUrl('/#/app1')
// setTimeout(() => {
//     // authToken.authToken = '222222222222';
//     authSubject.next('2222222222');
// }, 10000)
//authSubject.next('2222222222');
// 



function scriptTagAdd(url, globalVarName) {
    return new Promise((resolve, reject) => {
        const scriptEl = document.createElement('script');
        scriptEl.src = url;
        scriptEl.async = true;
        scriptEl.onload = () => resolve(window[globalVarName]);
        scriptEl.onerror = err => reject(err);
        document.head.appendChild(scriptEl);
    })
}

function navigateToApp1() {
    navigateToUrl('/#/app1');
}

function navigateToApp2() {
    navigateToUrl('/#/app2');
}