npm install

Before running this application, download my-app and my-app2 and follow their instructions.
Both my-app and my-app2 should be running separately on their own port, exposing their main.js files.

This app will be looking for:
	localhost:4200/main.js
	localhost:4201/main.js


To run this app:

npm start

navigate to: http://localhost:9090